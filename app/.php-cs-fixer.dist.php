<?php

declare(strict_types=1);

$finder = (new PhpCsFixer\Finder())
	->in(__DIR__)
	->exclude('var');

return (new PhpCsFixer\Config())
	->setRules([
		'@DoctrineAnnotation'                  => true,
		'@PHP80Migration:risky'                => true,
		'@PHP82Migration'                      => true,
		'@Symfony'                             => true,
		'@Symfony:risky'                       => true,
		'align_multiline_comment'              => true,
		'array_syntax'                         => [
			'syntax' => 'short',
		],
		'combine_consecutive_unsets'           => true,
		'compact_nullable_typehint'            => true,
		'doctrine_annotation_array_assignment' => [
			'operator' => ':',
		],
		'doctrine_annotation_braces'           => [
			'ignored_tags' => [
				'abstract', 'access', 'code', 'deprec', 'encode', 'exception', 'final', 'ingroup', 'inheritdoc', 'inheritDoc', 'magic', 'name', 'toc', 'tutorial', 'private', 'static', 'staticvar', 'staticVar', 'throw', 'api', 'author', 'category', 'copyright', 'deprecated', 'example', 'filesource', 'global', 'ignore', 'internal', 'license', 'link', 'method', 'package', 'param', 'property', 'property-read', 'property-write', 'return', 'see', 'since', 'source', 'subpackage', 'throws', 'todo', 'TODO', 'usedBy', 'uses', 'var', 'version', 'after', 'afterClass', 'backupGlobals', 'backupStaticAttributes', 'before', 'beforeClass', 'codeCoverageIgnore', 'codeCoverageIgnoreStart', 'codeCoverageIgnoreEnd', 'covers', 'coversDefaultClass', 'coversNothing', 'dataProvider', 'depends', 'expectedException', 'expectedExceptionCode', 'expectedExceptionMessage', 'expectedExceptionMessageRegExp', 'group', 'large', 'medium', 'preserveGlobalState', 'requires', 'runTestsInSeparateProcesses', 'runInSeparateProcess', 'small', 'test', 'testdox', 'ticket', 'uses', 'SuppressWarnings', 'noinspection', 'package_version', 'enduml', 'startuml', 'fix', 'FIXME', 'fixme', 'override',
				'psalm', 'template',
			],
			'syntax'       => 'with_braces',
		],
		'doctrine_annotation_indentation'      => [
			'indent_mixed_lines' => true,
		],
		'doctrine_annotation_spaces'           => [
			'after_argument_assignments'     => false,
			'after_array_assignments_colon'  => true,
			'around_commas'                  => true,
			'around_parentheses'             => true,
			'before_argument_assignments'    => false,
			'before_array_assignments_colon' => false,
		],

		'fully_qualified_strict_types'           => true,
		'linebreak_after_opening_tag'            => true,
		'list_syntax'                            => [
			'syntax' => 'short',
		],
		'logical_operators'                      => true,
		'multiline_comment_opening_closing'      => true,
		'multiline_whitespace_before_semicolons' => true,
		'no_alternative_syntax'                  => true,
		'no_binary_string'                       => true,
		'no_null_property_initialization'        => true,
		'no_php4_constructor'                    => true,
		'no_superfluous_phpdoc_tags'             => false,
		'no_unreachable_default_argument_value'  => true,
		'no_unset_on_property'                   => true,
		'no_useless_else'                        => true,
		'no_useless_return'                      => true,
		'ordered_class_elements'                 => true,
		'ordered_imports'                        => true,
		'php_unit_internal_class'                => true,
		'phpdoc_order'                           => true,
		'phpdoc_types_order'                     => true,
		'strict_comparison'                      => true,
		'visibility_required'                    => [
			'elements' => ['property', 'method', 'const'],
		],
		'void_return'                            => false,
		'@PSR2'                                  => true,
		'indentation_type'                       => true,
		'binary_operator_spaces'                 => [
			'operators' => [
				'=>' => 'align_single_space',
				'='  => 'align_single_space',
			],
		],
		'phpdoc_separation'                      => true,
		'phpdoc_to_comment'                      => [
			'ignored_tags' => [
				'psalm-suppress',
			],
		],
		'braces'                                 => [
			'allow_single_line_closure'                         => true,
			'allow_single_line_anonymous_class_with_empty_body' => true,
		],
	])
	->setIndent("\t")
	->setLineEnding("\n")
	->setRiskyAllowed(true)
	->setFinder($finder);
