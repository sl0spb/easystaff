<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

abstract class AbstractEntityRepository extends ServiceEntityRepository
{
	public function __construct(ManagerRegistry $registry)
	{
		parent::__construct($registry, $this->getEntityClass());
	}

	public function persist(object $entity): static
	{
		$this->_em->persist($entity);

		return $this;
	}

	public function remove(object $entity): static
	{
		$this->_em->remove($entity);

		return $this;
	}

	public function flush(): static
	{
		$this->_em->flush();

		return $this;
	}

	/**
	 * @template T of object
	 *
	 * @return class-string<T>
	 */
	abstract public function getEntityClass(): string;
}
