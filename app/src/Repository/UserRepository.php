<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\User;

class UserRepository extends AbstractEntityRepository
{
	public function getEntityClass(): string
	{
		return User::class;
	}
}
