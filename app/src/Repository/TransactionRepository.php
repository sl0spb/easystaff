<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Transaction;

class TransactionRepository extends AbstractEntityRepository
{
	public function getEntityClass(): string
	{
		return Transaction::class;
	}
}
