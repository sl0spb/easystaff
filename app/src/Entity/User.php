<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Trait\CreatedAtTrait;
use App\Entity\Trait\IdTrait;
use App\Entity\Trait\UpdatedAtTrait;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: UserRepository::class)]
class User
{
	use CreatedAtTrait;
	use IdTrait;
	use UpdatedAtTrait;

	#[ORM\Column(length: 255)]
	private ?string $name = null;

	/**
	 * @var Collection<int, Transaction>
	 */
	#[ORM\OneToMany(mappedBy: 'user', targetEntity: Transaction::class, cascade: ['persist', 'delete'], orphanRemoval: true)]
	private Collection $transactions;

	public function __construct()
	{
		$this->transactions = new ArrayCollection();
	}

	public function getName(): ?string
	{
		return $this->name;
	}

	public function setName(string $name): self
	{
		$this->name = $name;

		return $this;
	}

	/**
	 * @return Collection<int, Transaction>
	 */
	public function getTransactions(): Collection
	{
		return $this->transactions;
	}

	public function addTransaction(Transaction $transaction): self
	{
		if (!$this->transactions->contains($transaction)) {
			$this->transactions->add($transaction);
		}

		return $this;
	}

	public function removeTransaction(Transaction $transaction): self
	{
		$this->transactions->removeElement($transaction);

		return $this;
	}
}
