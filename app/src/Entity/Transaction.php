<?php

declare(strict_types=1);

namespace App\Entity;

use App\Entity\Trait\CreatedAtTrait;
use App\Entity\Trait\IdTrait;
use App\Repository\TransactionRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
class Transaction
{
	use CreatedAtTrait;
	use IdTrait;

	#[ORM\ManyToOne(targetEntity: User::class)]
	#[ORM\JoinColumn(
		name: 'user_id',
		referencedColumnName: 'id',
		nullable: false,
		onDelete: 'CASCADE'
	)]
	protected User $user;

	public function getUser(): User
	{
		return $this->user;
	}

	public function setUser(User $user): self
	{
		$this->user = $user;

		return $this;
	}
}
