<?php

declare(strict_types=1);

namespace App\Entity\Trait;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait CreatedAtTrait
{
	#[ORM\Column(type: 'datetime_immutable', nullable: true)]
	#[Gedmo\Timestampable(on: 'create')]
	protected ?\DateTimeInterface $created_at = null;

	public function getCreatedAt(): ?\DateTimeInterface
	{
		return $this->created_at;
	}

	public function setCreatedAt(\DateTimeInterface $createdAt): static
	{
		$this->created_at = $createdAt;

		return $this;
	}
}
