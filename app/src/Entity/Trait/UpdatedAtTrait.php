<?php

declare(strict_types=1);

namespace App\Entity\Trait;

use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;

trait UpdatedAtTrait
{
	#[ORM\Column(type: 'datetime_immutable', nullable: true)]
	#[Gedmo\Timestampable(on: 'update')]
	protected ?\DateTimeInterface $updatedAt = null;

	public function getUpdatedAt(): ?\DateTimeInterface
	{
		return $this->updatedAt;
	}

	public function setUpdatedAt(\DateTimeInterface $lastActivity): static
	{
		$this->updatedAt = $lastActivity;

		return $this;
	}
}
