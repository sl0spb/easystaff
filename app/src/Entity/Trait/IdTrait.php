<?php

declare(strict_types=1);

namespace App\Entity\Trait;

use Doctrine\ORM\Mapping as ORM;

trait IdTrait
{
	#[ORM\Id]
	#[ORM\GeneratedValue('IDENTITY')]
	#[ORM\Column(type: 'integer')]
	protected ?int $id = null;

	public function getId(): int
	{
		if (null === $this->id) {
			throw new \RuntimeException(sprintf('%s::%s must not be called on a non persisted entity.', static::class, __METHOD__));
		}

		return $this->id;
	}
}
