up: docker-up
down: docker-down
restart: docker-down docker-up
init:  docker-down-clear docker-pull docker-build-app docker-up initialization

docker-up:
	docker compose up -d

docker-down:
	 docker compose down --remove-orphans

docker-down-clear:
	 docker compose down -v --remove-orphans

docker-pull:
	docker compose pull

docker-build-app:
	 docker compose build --build-arg target=app_dev

initialization:
	docker compose exec app sh /tmp/init.sh

fixer:
	docker compose exec app vendor/bin/php-cs-fixer fix

psalm:
	docker compose exec app vendor/bin/psalm --show-info=true --no-cache