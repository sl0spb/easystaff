#!/usr/bin/env sh

composer install --no-scripts &&
  bin/console doctrine:database:create --if-not-exists --no-interaction &&
  bin/console doctrine:migrations:migrate --allow-no-migration --no-interaction &&
  bin/console hautelook:fixtures:load --no-interaction
